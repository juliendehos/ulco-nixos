{ pkgs, ... }:

{

  programs.vscode = {
    enable = true;

    extensions = with pkgs.vscode-extensions; [

      bbenoist.nix
      ms-vscode.cpptools
      # ms-vscode.cpptools-extension-pack
      ms-python.python

      (pkgs.vscode-utils.buildVscodeMarketplaceExtension {
        mktplcRef = {
          name = "nix-env-selector";
          publisher = "arrterian";
          version = "1.0.11";
          sha256 = "sha256-dK0aIH8tkG/9UGblNO0WwxJABBEEKEy4nSmIwdDpf4Q=";
        };
      })

      (pkgs.vscode-utils.buildVscodeMarketplaceExtension {
        mktplcRef = {
          name = "cmake-tools";
          publisher = "ms-vscode";
          version = "1.19.16";
          sha256 = "sha256-XWyFd/KNhtFpqtw+pbu2DyTkcMfoRClrpTvmq+lEl6U=";
        };
      })

      (pkgs.vscode-utils.buildVscodeMarketplaceExtension {
        mktplcRef = {
          name = "cmake";
          publisher = "twxs";
          version = "0.0.17";
          sha256 = "sha256:11hzjd0gxkq37689rrr2aszxng5l9fwpgs9nnglq3zhfa1msyn08";
        };
      })

      (pkgs.vscode-utils.buildVscodeMarketplaceExtension {
        mktplcRef = {
          name = "vscode-ghc-simple";
          publisher = "dramforever";
          version = "0.2.4";
          sha256 = "sha256-CMECeHvUCCpJG2gUk9FvbUF8T2+04ngWDLgPMsR1FjQ=";
        };
      })

      (pkgs.vscode-utils.buildVscodeMarketplaceExtension {
        mktplcRef = {
          name = "language-haskell";
          publisher = "JustusAdam";
          version = "3.6.0";
          sha256 = "sha256-rZXRzPmu7IYmyRWANtpJp3wp0r/RwB7eGHEJa7hBvoQ=";
        };
      })

        (pkgs.vscode-utils.buildVscodeMarketplaceExtension {
          mktplcRef = {
            name = "todo-tree";
            publisher = "Gruntfuggly";
            version = "0.0.226";
            sha256 = "sha256-Fj9cw+VJ2jkTGUclB1TLvURhzQsaryFQs/+f2RZOLHs=";
          };
        })

    ];
  };
}

