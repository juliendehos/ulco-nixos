{ pkgs, ... }:

  {

  home.packages = with pkgs; [

    kickstart

    (haskellPackages.ghcWithPackages (ps: with ps; [
      haskell-gi-base
      hspec
      gi-cairo
      gi-gdk
      gi-glib
      gi-gtk
      gi-cairo-render
      gi-cairo-connector
      # QuickCheck
      random
      scotty
      split
      text
    ]))

    # (python3.withPackages (ps: with ps; [
    #   pygments
    #   sphinx
    #   sphinx_rtd_theme
    #   rstcheck
    # ]))

    anki

    #baobab

    # cabal-install
    cmake
    # ctags

    doxygen_gui

    evince
    eog

    file

    gcc
    # gdb
    # gimp
    git
    gnumake
    gnuplot
    
    hlint
    htop

    imagemagick

    killall
    kcachegrind

    libreoffice

    mdbook
    meld

    # nodejs
    # nerdfonts

    # pavucontrol
    # pkg-config

    # ranger

    sqlite
    sqlitebrowser

    # tmux
    # tokei
    tree

    unzip

    valgrind
    vlc
    vim

    wget

    xclip
    xsel
    xorg.xkill

  ];

}

