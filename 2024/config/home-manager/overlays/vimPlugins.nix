self: super: {

  vimPlugins = super.vimPlugins // {

    # minibufexpl = self.vimUtils.buildVimPluginFrom2Nix { 
    #   name = "minibufexpl.vim-2013-06-16";
    #   src = self.fetchgit {
    #     url = "https://github.com/fholgado/minibufexpl.vim";
    #     rev = "ad72976ca3df4585d49aa296799f14f3b34cf953";
    #     sha256 = "1bfq8mnjyw43dzav8v1wcm4rrr2ms38vq8pa290ig06247w7s7ng";
    #   };
    #   dependencies = [];
    # };

    badwolf = self.vimUtils.buildVimPlugin { 
        name = "badwolf";
        src = self.fetchgit {
            url = "https://github.com/sjl/badwolf";
            rev = "v1.6.0";
            sha256 = "1hywj43ww2wz0z0mrhvlzkifrvswnwgbnnr0hbrg8vs5d131ihg6";
        };
        dependencies = [];
    };

    nvim-tree = self.vimUtils.buildVimPlugin { 
        name = "nvim-tree";
        src = self.fetchgit {
            url = "https://github.com/nvim-tree/nvim-tree.lua";
            rev = "nvim-tree-v1.4.0";
            sha256 = "sha256-7KO3wPW65IH4m0jEoyFScNiAVwrlNHU+p0H55AuwlWk=";
        };
        dependencies = [];
    };

    bufferline = self.vimUtils.buildVimPlugin { 
        name = "bufferline";
        src = self.fetchgit {
            url = "https://github.com/akinsho/bufferline.nvim";
            rev = "v4.6.1";
            sha256 = "sha256-8PCkY1zrlMrPGnQOb7MjqDXNlkeX46jrT4ScIL+MOwM=";
        };
        dependencies = [];
    };

    scope = self.vimUtils.buildVimPlugin { 
        name = "scope";
        src = self.fetchgit {
            url = "https://github.com/tiagovla/scope.nvim";
            rev = "86a0f5b";
            sha256 = "sha256-8Bd1DI6nuUxkTCx0wCfWKHdi6UXQNuDygj5UuSGypLs=";
        };
        dependencies = [];
    };

    hop = self.vimUtils.buildVimPlugin { 
        name = "hop";
        src = self.fetchgit {
            url = "https://github.com/phaazon/hop.nvim";
            rev = "v2.0.3";
            sha256 = "sha256-UZZlo5n1x8UfM9OP7RHfT3sFRfMpLkBLbEdcSO+SU6E=";
        };
        dependencies = [];
    };

    lualine = self.vimUtils.buildVimPlugin { 
        name = "lualine";
        src = self.fetchgit {
            url = "https://github.com/nvim-lualine/lualine.nvim";
            rev = "6a40b53";
            sha256 = "sha256-FGzK9K8yOPbq8DYK8Efu4MZ20p3JNuovbONwgnnK9J4=";
        };
        dependencies = [];
    };

    devicons = self.vimUtils.buildVimPlugin { 
        name = "devicons";
        src = self.fetchgit {
            url = "https://github.com/nvim-tree/nvim-web-devicons";
            rev = "v0.100";
            sha256 = "sha256-DSUTxUFCesXuaJjrDNvurILUt1IrO5MI5ukbZ8D87zQ=";
        };
        dependencies = [];
    };

    gitsigns = self.vimUtils.buildVimPlugin { 
        name = "gitsigns";
        src = self.fetchgit {
            url = "https://github.com/lewis6991/gitsigns.nvim";
            rev = "v0.9.0";
            sha256 = "sha256-AbnjBqKLhOGMGBXBnu9zbL3PG7rKmAoYtxY17kzFEIA=";
        };
        dependencies = [];
    };

    toggleterm = self.vimUtils.buildVimPlugin { 
        name = "toggleterm";
        src = self.fetchgit {
            url = "https://github.com/akinsho/toggleterm.nvim";
            rev = "v2.11.0";
            sha256 = "sha256-mM5bGgAemsRJD9U6U5K6ia5qb8NaTusM99x6xrtEBfw=";
        };
        dependencies = [];
    };

  };

}

