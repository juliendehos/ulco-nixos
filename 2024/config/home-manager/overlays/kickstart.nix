self: super: {

  kickstart = self.rustPlatform.buildRustPackage rec {
    pname = "kickstart";
    version = "v0.4.0";

    src = self.fetchFromGitHub {
      owner = "Keats";
      repo = pname;
      rev = version;
      sha256 = "sha256-GIBSHPIUq+skTx5k+94/K1FJ30BCboWPA6GadgXwp+I=";
    };

    doCheck = false;
    cargoHash = "sha256-u88oKuiSm3ordroM9afgAx0rMarozfTsNocXHUmRTFE=";

  };

}

