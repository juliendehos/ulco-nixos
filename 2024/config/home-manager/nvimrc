set nocompatible
" set nofoldenable

" Leader
let mapleader = ","

" color
set termguicolors
syntax on
colorscheme badwolf
let g:badwolf_darkgutter = 1
let g:badwolf_tabline = 0
set guifont=Monospace:h9

" display options
set number
set cursorline
set colorcolumn=80
set ruler         " show the cursor position all the time
set showcmd       " display incomplete command

" search
set hlsearch
set incsearch     " do incremental searching
set ignorecase    " case insensitive searching (unless specified)
set smartcase
" set nowrapscan

" backup and undo
set undofile
set history=1000

" completion
set wildmode=longest:full,full
set wildmenu

" mouse and scrolling
set mouse=a
set scrolloff=4 

" tabulation
set tabstop=2
set expandtab
set shiftwidth=2

" Open new split panes to right and bottom, which feels more natural
set splitbelow
set splitright

" HTML Editing
set matchpairs+=<:>

" Quicker window movement
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-h> <C-w>h
nnoremap <C-l> <C-w>l

" Always use vertical diffs
set diffopt+=vertical

" resize panes
nnoremap <silent> <Right> :vertical resize +5<cr>
nnoremap <silent> <Left> :vertical resize -5<cr>
nnoremap <silent> <Up> :resize +5<cr>
nnoremap <silent> <Down> :resize -5<cr>

" navigate through compilation errors
nmap <F7> :cp<cr>
nmap <F8> :cn<cr>

" delete buffer without losing the split window
nnoremap <C-c> :bp\|bd #<CR>

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" plugins
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

lua << EOF
require('bufferline').setup{}
require('scope').setup{}
require('hop').setup{}

require('lualine').setup()
-- require('feline').setup()
-- require('gitsigns').setup()

require'nvim-web-devicons'.setup {
 -- your personnal icons can go here (to override)
 -- you can specify color or cterm_color instead of specifying both of them
 -- DevIcon will be appended to `name`
 -- override = {
 -- };
 -- globally enable different highlight colors per icon (default to true)
 -- if set to false all icons will have the default icon's color
 color_icons = true;
 -- globally enable default icons (default to false)
 -- will get overriden by `get_icons` option
 default = true;
}

-- nvim-tree
-- disable netrw at the very start of your init.lua (strongly advised)
vim.g.loaded_netrw = 1
vim.g.loaded_netrwPlugin = 1
-- set termguicolors to enable highlight groups
-- vim.opt.termguicolors = true
-- empty setup using defaults
require("nvim-tree").setup{
  actions = {
    open_file = {
      resize_window = false
      }
    }
}

require("toggleterm").setup()
function _G.set_terminal_keymaps()
  local opts = {buffer = 0}
  vim.keymap.set('t', '<esc>', [[<C-\><C-n>]], opts)
  -- vim.keymap.set('t', 'jk', [[<C-\><C-n>]], opts)
  vim.keymap.set('t', '<C-h>', [[<Cmd>wincmd h<CR>]], opts)
  vim.keymap.set('t', '<C-j>', [[<Cmd>wincmd j<CR>]], opts)
  vim.keymap.set('t', '<C-k>', [[<Cmd>wincmd k<CR>]], opts)
  vim.keymap.set('t', '<C-l>', [[<Cmd>wincmd l<CR>]], opts)
end
-- if you only want these mappings for toggle term use term://*toggleterm#* instead
vim.cmd('autocmd! TermOpen term://* lua set_terminal_keymaps()')
EOF

"" lean
""let maplocalleader = ',,'

"" ale
let g:ale_completion_enabled = 1
let g:ale_linters = {'haskell': ['hlint', 'ghc']}
let g:ale_haskell_ghc_options = '-fno-code -v0 -isrc -Wall'
let g:ale_cursor_detail = 0
let g:ale_lint_on_text_changed = 'never'
let g:ale_lint_on_insert_leave = 0

""let g:ale_set_highlights = 0
""let g:ale_set_signs = 1
""let g:ale_cpp_gcc_options = '--std=c++17 -Wall'
""let g:ale_cpp_options = '--std=c++17 -Wall'
""let g:ale_c_cc_options = '--std=c++17 -Wall'


"" keymap
nmap <F2> :bprev<cr>
nmap <F3> :bnext<cr>
nmap <F4> :NvimTreeToggle<CR>
nmap <F5> :HopWord<CR>
nmap <F6> :ToggleTerm<CR>
nmap <F9> <Plug>(ale_previous_wrap)
nmap <F12> <Plug>(ale_next_wrap)
nnoremap <C-w> :bp\|bd #<CR>

"" neovide
if exists('g:neovide')
  let g:neovide_scale_factor=0.5

  function! ChangeScaleFactor(delta)
      let g:neovide_scale_factor = g:neovide_scale_factor * a:delta
  endfunction

  nnoremap <expr><C-=> ChangeScaleFactor(1.25)
  nnoremap <expr><C--> ChangeScaleFactor(1/1.25)
endif

"" map! <S-Insert> <C-R>+
set clipboard=unnamedplus
"" install xsel or xclip

" system clipboard
" https://github.com/neovide/neovide/issues/113
"" nmap <c-c> "+y
"" vmap <c-c> "+y
"" nmap <c-v> "+p
"" vmap <c-v> "+p
inoremap <c-v> <c-r>+
cnoremap <c-v> <c-r>+
" use <c-r> to insert original character without triggering things like auto-pairs
inoremap <c-r> <c-v>

