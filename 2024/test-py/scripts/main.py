import mypackage.hello as hello

if __name__ == '__main__':
    
    x = 21
    print(f'mul2({x}) = {hello.mul2(x)}')

# nix-shell --run "python scripts/main.py"

