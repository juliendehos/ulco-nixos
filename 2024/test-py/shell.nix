{ pkgs ? import <nixpkgs> {} }:

let
  hello = pkgs.python3.pkgs.buildPythonPackage {
    pname = "hello";
    version = "0.1.0";
    src = ./.;
    propagatedBuildInputs = [
    ];
  };

in pkgs.mkShell {
  packages = [
    (pkgs.python3.withPackages (ps: [ hello ]))
  ];
}

