
## nixos

```
sudo su

cfdisk /dev/sda
# table dos
# partition 1 : 2Go swap
# partition 2 : 33Go linux

mkfs.ext4 -L nixos /dev/sda2
mount /dev/sda2 /mnt

mkswap -L swap /dev/sda1
swapon /dev/sda1

nixos-generate-config --root /mnt
nano /mnt/etc/nixos/configuration.nix
nixos-install

poweroff
# ejecter l'iso


passwd toto
```

## flake 

```nix
# /etc/nixos/configuration.nix

nix = {
  package = pkgs.nixFlakes;
  extraOptions = ''
    experimental-features = nix-command flakes
  '';
};
```

```sh
# sudo nixos-rebuild switch
```

```sh
$ nix run home-manager/release-24.05 -- init --switch
```

```nix
# ~/.config/home-manager/flake.nix

pkgs = import nixpkgs {
  inherit system;
  config.allowUnfree = true;
};
```

## home-manager

```nix
# ~/.config/home-manager/home.nix

  nixpkgs.overlays = [ 
    (import ./overlays/kickstart.nix)
    (import ./overlays/vimPlugins.nix)
  ];
```

~/.config/home-manager/...

```sh
$ home-manager switch
```

## vscode

### settings

- trust empty window
- trust enabled

- extensions auto check updates
- update mode

- todo-tree regex -> haskell


### key bindings

- toggle terminal -> ctrl+;


## git

```
git config --global credential.helper "cache --timeout=86400"
git config --global pull.rebase false
```

## xfce

- verrouillage ecran

## firefox

- search

## anki

- installer les flash-cards


