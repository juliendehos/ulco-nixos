# Edit this configuration file to define what should be installed on
# your system. Help is available in the configuration.nix(5) man page, on
# https://search.nixos.org/options and in the NixOS manual (`nixos-help`).

{ config, lib, pkgs, ... }:

{
  imports = [ ./hardware-configuration.nix ];

  boot.initrd.checkJournalingFS = false;
  boot.loader.grub = {
    enable = true;
    device = "/dev/sda"; 
  };

  nix.settings.trusted-users = [ "toto" "root" ];

  nix = {
    package = pkgs.nixFlakes;
    extraOptions = ''
      experimental-features = nix-command flakes
    '';
  };

  virtualisation.docker.enable = true;
  virtualisation.virtualbox.guest.enable = true;

  networking.hostName = "nixos"; 
  networking.networkmanager.enable = true;  

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  time.timeZone = "Europe/Paris";

  # Select internationalisation properties.
  i18n.defaultLocale = "fr_FR.UTF-8";
  # console = {
  #  font = "Lat2-Terminus16";
  #  keyMap = "us";
  #  useXkbConfig = true; # use xkb.options in tty.
  #};

  # Enable the X11 windowing system.
  services = {

    xserver = {
      enable = true;
      desktopManager.xfce.enable = true;
      xkb = {
        # layout = "fr";
        layout = "us";
        options = "caps:escape";
      };
    };

    displayManager = {
      defaultSession = "xfce";
      autoLogin = { 
        enable = true;
        user = "toto";
      };
    };

    # Enable touchpad support (enabled default in most desktopManager).
    libinput.enable = true;

  };
 
  # Enable sound.
  # hardware.pulseaudio.enable = true;
  # OR
  # services.pipewire = {
  #   enable = true;
  #   pulse.enable = true;
  # };

  users.users.toto = {
    isNormalUser = true;
    extraGroups = [ "networkmanager" "wheel" "docker" ];
   # packages = with pkgs; [ ];
  };

  environment.systemPackages = with pkgs; [ file firefox git tree vim ];

  system.stateVersion = "24.05"; 

}

