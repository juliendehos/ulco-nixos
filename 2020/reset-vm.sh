#!/bin/sh

VBoxManage setproperty machinefolder /usr/local/VM/$USER
VBoxManage natnetwork add --netname NatNetwork --network "10.0.2.0/24"
VBoxManage unregistervm --delete nixos
VBoxManage import /usr/local/VM/nixos.ova
VBoxManage modifyvm nixos --nic1 natnetwork --nicpromisc1 allow-vms

# https://www.virtualbox.org/manual/ch08.html

