{ pkgs }: 

with pkgs; vscode-with-extensions.override {
  vscodeExtensions = with vscode-extensions; [

    ms-vscode.cpptools

    bbenoist.Nix

    (vscode-utils.buildVscodeMarketplaceExtension {
      mktplcRef = {
        name = "nix-env-selector";
        publisher = "arrterian";
        version = "0.1.2";
        sha256 = "1n5ilw1k29km9b0yzfd32m8gvwa2xhh6156d4dys6l8sbfpp2cv9";
      };
    })

    (vscode-utils.buildVscodeMarketplaceExtension {
      mktplcRef = {
        name = "cmake-tools";
        publisher = "ms-vscode";
        version = "1.4.1";
        sha256 = "18hj94p3003cba141smirckpsz56cg3fabb8il2mx1xzbqlx2xhk";
      };
    })

    (vscode-utils.buildVscodeMarketplaceExtension {
      mktplcRef = {
        name = "cmake";
        publisher = "twxs";
        version = "0.0.17";
        sha256 = "11hzjd0gxkq37689rrr2aszxng5l9fwpgs9nnglq3zhfa1msyn08";
      };
    })

    # (vscode-utils.buildVscodeMarketplaceExtension {
    #   mktplcRef = {
    #     name = "ghcide";
    #     publisher = "DigitalAssetHoldingsLLC";
    #     version = "0.0.2";
    #     sha256 = "02gla0g11qcgd6sjvkiazzk3fq104b38skqrs6hvxcv2fzvm9zwf";
    #   };
    # })

    (vscode-utils.buildVscodeMarketplaceExtension {
      mktplcRef = {
        name = "vscode-ghc-simple";
        publisher = "dramforever";
        version = "0.1.22";
        sha256 = "0x3csdn3pz5rhl9mhplpm8kxb40l1dw5rnwhh3zsif3rz0nqhk2a";
      };
    })

    (vscode-utils.buildVscodeMarketplaceExtension {
      mktplcRef = {
        name = "language-haskell";
        publisher = "JustusAdam";
        version = "1.2.0";
        sha256 = "1gdm9wm446s2dkv2p4dpv3hs5pcvsh3ldvlv7nn84pjzg7l3sfch";
      };
    })

    (vscode-utils.buildVscodeMarketplaceExtension {
      mktplcRef = {
        name = "restructuredtext";
        publisher = "lextudio";
        version = "128.0.0";
        sha256 = "16cs2bqhmkx2prdnkm6fy3qg1xqiyy445wr4l8y2ls9c61ax2cjm";
      };
    })

    (vscode-utils.buildVscodeMarketplaceExtension {
      mktplcRef = {
        name = "todo-tree";
        publisher = "Gruntfuggly";
        version = "0.0.177";
        sha256 = "1j3vgyimc4gamp3dnym9wfk445q5hipjq3cimvpqqa22pk4g0224";
      };
    })

  ];
}

