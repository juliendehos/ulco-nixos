{ pkgs }:

let

    _minibufexpl = pkgs.vimUtils.buildVimPluginFrom2Nix { 
        name = "minibufexpl.vim-2013-06-16";
        src = pkgs.fetchgit {
            url = "https://github.com/fholgado/minibufexpl.vim";
            rev = "ad72976ca3df4585d49aa296799f14f3b34cf953";
            sha256 = "1bfq8mnjyw43dzav8v1wcm4rrr2ms38vq8pa290ig06247w7s7ng";
        };
        dependencies = [];
    };

in

pkgs.vim_configurable.customize {

  name = "vim";

  vimrcConfig.customRC = builtins.readFile ./vimrc;

  vimrcConfig.packages.myVimPackage = with pkgs.vimPlugins; {
    start = [ 

      julia-vim
      nim-vim

      airline
      ale
      easymotion
      _minibufexpl
      nerdtree
      tagbar
      vim-colorschemes
      vim-nix

    ];
  };

}


