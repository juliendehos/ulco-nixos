{ pkgs } :

pkgs.haskellPackages.ghcWithPackages (ps: with ps; [
  haskell-gi-base
  hspec
  gi-cairo
  gi-gdk
  gi-glib
  gi-gtk
  gi-cairo-render
  gi-cairo-connector
  QuickCheck
  random
  scotty
  text
])

