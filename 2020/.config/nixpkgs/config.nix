{

  allowUnfree = true;

  packageOverrides = pkgs: with pkgs; {

    myPackages = pkgs.buildEnv {
      name = "myPackages";
      paths = [

        ##################################################
        # callPackage
        ##################################################
        (callPackage ./kickstart.nix {})
        (callPackage ./ghc.nix {})
        (callPackage ./python.nix {})
        (callPackage ./vim.nix {})
        (callPackage ./vscode.nix {})
        # (import (builtins.fetchTarball "https://github.com/cachix/ghcide-nix/tarball/master") {}).ghcide-ghc865

        ##################################################
        # packages
        ##################################################
        cabal-install
        clang
        cmakeWithGui
        doxygen_gui
        #electron
        evince
        gdb
        geany
        gnome3.eog
        gimp
        gnumake
        gnuplot
        gtkmm3
        heroku
        imagemagick
        kcachegrind
        libreoffice
        llvm
        meld
        nodejs-12_x
        #pkgconfig
        qt5Full
        #qtcreator
        sqlite
        sqlitebrowser
        valgrind
        vlc

      ];
    };

  };
}

