{ pkgs }:

pkgs.python3.withPackages (ps: with ps; [
  pygments
  sphinx
  #sphinx_rtd_theme

  (buildPythonPackage rec {
    pname = "rstcheck";
    version = "3.3.1";
    src = pkgs.fetchFromGitHub {
      owner = "myint";
      repo = "${pname}";
      rev = "v${version}";
      sha256 = "0wl5mlc7b8sifn5s3c5wv0ga1b99xf7ni6ig186dabpywhv48270";
    };
    doCheck = false;
    propagatedBuildInputs = [ docutils ];
  })

])
