self: super: {

  opencv_gtk = super.opencv.override {
    enableGtk2 = true; 
    enableGStreamer = false;
    enableFfmpeg = true;
    enableTIFF = false;
    enableEXR = false;
    enableJPEG2K = false;
    enableEigen = false;
  };

}

