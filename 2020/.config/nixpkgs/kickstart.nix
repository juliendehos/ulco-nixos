{ pkgs }:

pkgs.rustPlatform.buildRustPackage rec {
  pname = "kickstart";
  version = "v0.2.1";

  src = pkgs.fetchFromGitHub {
    owner = "Keats";
    repo = pname;
    rev = version;
    sha256 = "0imrwkqgaafn2g94wqwfyi5ldi0h09kg5fq8fm9g0p3qc2xbfwr5";
  };

  doCheck = false;
  cargoSha256 = "04z4c44x0k1xbsm7k120hmmr1xmshkdn576l7ij9dpnsv5jn01i6";

}

