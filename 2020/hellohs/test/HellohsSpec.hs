
module HellohsSpec (main, spec) where

import Test.Hspec
import Test.QuickCheck

import Hellohs

main :: IO ()
main = hspec spec

spec :: Spec
spec = do
    describe "HellohsSpec" $ do
        it "TODO" $ property $ \x -> add42 x == (x+42)
