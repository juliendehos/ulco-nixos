# pour prendre en compte les modifications, faire un :
# sudo nixos-rebuild switch

{ config, pkgs, ... }: {

  nix.trustedUsers = [ "toto" "root" ];
  users.extraUsers.toto = {
    extraGroups = [ "wheel" "docker" ];
    isNormalUser = true;
  };

  imports = [ ./hardware-configuration.nix ];

  time.timeZone = "Europe/Paris";
  virtualisation.docker.enable = true;
  virtualisation.virtualbox.guest.enable = true;

  networking = {
    hostName = "nixos";
    useDHCP = false;
    interfaces.enp0s3.useDHCP = true;

    #proxy = {
    #  default = "TODO:TODO@192.168.22.62:3128"; 
    #  noProxy = "127.0.0.1,localhost"; 
    #};

    #firewall.allowedTCPPortRanges = [ { from = 8000; to = 8080; } ];
  };

  boot.loader.grub = {
    device = "/dev/sda"; 
    enable = true;
    version = 2;
  };

  console = {
    font = "Lat2-Terminus16";
    keyMap = "fr";
    # keyMap = "fr-mac";
    # keyMap = "fr-bepo";
  };
  i18n.defaultLocale = "fr_FR.UTF-8";

  sound.enable = true;
  hardware.pulseaudio.enable = true;

  environment.systemPackages = with pkgs; [
    cachix
    chromium
    firefox 
    file
    git 
    htop
    pavucontrol
    tmux
    tree
    vim 
  ];

  services = {

    dbus = {
      enable = true;
      packages = [ pkgs.gnome3.dconf ];
    };

    # postgresql = {
    #   enable = true;
    #   authentication = ''
    #     local all all trust
    #   '';
    # };

    xserver = {
      desktopManager.xfce.enable = true;
      displayManager.lightdm.enable = true;
      enable = true;
      layout = "fr";
      # xkbVariant = "mac";
      # xkbVariant = "bepo";
    };

  };

  # This value determines the NixOS release with which your system is to be
  # compatible, in order to avoid breaking some software such as database
  # servers. You should change this only after NixOS release notes say you
  # should.
  system.stateVersion = "20.03";

}

