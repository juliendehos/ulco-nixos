
```sh
git clone https://gitlab.com/juliendehos/ulco-nixos.git
cd ulco-nixos

sudo cp etc/nixos/configuration.nix /etc/nixos/
sudo nixos-rebuild switch

cp -r .config/* ~/.config/
cachix use ghcide-nix
cachix use miso-haskell
nix-env -iA nixos.myPackages
nix-env -iA nixos.opencv_gtk
```

```sh
git config --global user.name "John Doe"
git config --global user.email doe@nimpe.org
git config --global credential.helper 'cache --timeout=10800'
```

