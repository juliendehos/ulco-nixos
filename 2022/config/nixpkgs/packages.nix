{ pkgs, ... }:

  {

  home.packages = with pkgs; [

    kickstart

    (haskellPackages.ghcWithPackages (ps: with ps; [
      haskell-gi-base
      hspec
      gi-cairo
      gi-gdk
      gi-glib
      gi-gtk
      gi-cairo-render
      gi-cairo-connector
      QuickCheck
      random
      scotty
      split
      text
    ]))

    (python3.withPackages (ps: with ps; [
      pygments
      sphinx
      sphinx_rtd_theme
      rstcheck
    ]))


    baobab

    cabal-install
    cmake
    ctags

    doxygen_gui

    evince

    gcc
    gdb
    gimp
    gnome3.eog
    gnumake
    gnuplot
    
    heroku
    hlint

    imagemagick

    killall
    kcachegrind

    libreoffice

    meld

    # nodejs

    opencv_gtk

    pkg-config

    ranger
    ripgrep

    sqlite
    sqlitebrowser

    tokei

    unzip

    valgrind
    vlc

    xorg.xkill

  ];

}

