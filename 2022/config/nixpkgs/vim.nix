{ pkgs, ... }:

{

  programs.vim = {

    enable = true;

    plugins = with pkgs.vimPlugins; [
      airline
      ale
      badwolf
      easymotion
      minibufexpl
      nerdtree
      nim-vim
      tagbar
      vim-colorschemes
      vim-nix
    ];

    extraConfig = builtins.readFile ./vimrc;
  };

}

