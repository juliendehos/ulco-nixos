{ pkgs, ... }:

{

  programs.vscode = {
    enable = true;

    extensions = with pkgs.vscode-extensions; [

      bbenoist.nix
      ms-vscode.cpptools
      ms-python.python

      (pkgs.vscode-utils.buildVscodeMarketplaceExtension {
        mktplcRef = {
          name = "nix-env-selector";
          publisher = "arrterian";
          version = "1.0.7";
          sha256 = "sha256:0mralimyzhyp4x9q98x3ck64ifbjqdp8cxcami7clvdvkmf8hxhf";
        };
      })

      (pkgs.vscode-utils.buildVscodeMarketplaceExtension {
        mktplcRef = {
          name = "cmake-tools";
          publisher = "ms-vscode";
          version = "1.7.3";
          sha256 = "sha256:0jisjyk5n5y59f1lbpbg8kmjdpnp1q2bmhzbc1skq7fa8hj54hp9";
        };
      })

      (pkgs.vscode-utils.buildVscodeMarketplaceExtension {
        mktplcRef = {
          name = "cmake";
          publisher = "twxs";
          version = "0.0.17";
          sha256 = "sha256:11hzjd0gxkq37689rrr2aszxng5l9fwpgs9nnglq3zhfa1msyn08";
        };
      })

      (pkgs.vscode-utils.buildVscodeMarketplaceExtension {
        mktplcRef = {
          name = "vscode-ghc-simple";
          publisher = "dramforever";
          version = "0.2.3";
          sha256 = "sha256:1pd7p4xdvcgmp8m9aymw0ymja1qxvds7ikgm4jil7ffnzl17n6kp";
        };
      })

      (pkgs.vscode-utils.buildVscodeMarketplaceExtension {
        mktplcRef = {
          name = "language-haskell";
          publisher = "JustusAdam";
          version = "3.4.0";
          sha256 = "sha256:0ab7m5jzxakjxaiwmg0jcck53vnn183589bbxh3iiylkpicrv67y";
        };
      })

      (pkgs.vscode-utils.buildVscodeMarketplaceExtension {
        mktplcRef = {
          name = "restructuredtext";
          publisher = "lextudio";
          version = "155.0.0";
          sha256 = "sha256:04jqywywxmhpvxz1nxd2hamjd9sg880r4g1wxaz2x4f51z0xda8k";
        };
      })

        (pkgs.vscode-utils.buildVscodeMarketplaceExtension {
          mktplcRef = {
            name = "todo-tree";
            publisher = "Gruntfuggly";
            version = "0.0.215";
            sha256 = "sha256-WK9J6TvmMCLoqeKWh5FVp1mNAXPWVmRvi/iFuLWMylM=";
          };
        })

    ];
  };
}

