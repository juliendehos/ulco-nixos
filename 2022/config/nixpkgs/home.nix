{ config, pkgs, ... }:

{

  imports = [
    ./git.nix
    ./packages.nix
    ./vim.nix
    ./vscode.nix
  ];

  programs = {
    home-manager.enable = true;
  };

  home.username = "toto";
  home.homeDirectory = "/home/toto";
  home.stateVersion = "22.05";

  # home.keyboard.options = [ "caps:swapescape" ];

}

