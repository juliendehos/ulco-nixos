{ config, pkgs, ... }:

{
  imports = [ ./hardware-configuration.nix ];

  nix.trustedUsers = [ "toto" "root" ];

  users.extraUsers.toto = {
    extraGroups = [ 
      "audio" 
      "docker" 
      "video" 
      "wheel" 
    ];
    isNormalUser = true;
  };

  virtualisation.docker.enable = true;
  virtualisation.virtualbox.guest.enable = true;

  boot.loader.grub = {
    enable = true;
    version = 2;
    device = "/dev/sda"; 
  };

  boot.initrd.checkJournalingFS = false;

  networking = {
    hostName = "nixos"; 
    useDHCP = false;
    interfaces.enp0s3.useDHCP = true;

    #proxy = {
    #  default = "TODO:TODO@192.168.22.62:3128"; 
    #  noProxy = "127.0.0.1,localhost"; 
    #};

    #firewall.allowedTCPPortRanges = [ { from = 8000; to = 8080; } ];

  };

  time.timeZone = "Europe/Paris";

  i18n.defaultLocale = "fr_FR.UTF-8";
  console = {
    font = "Lat2-Terminus16";
    keyMap = "us";
    # keyMap = "fr";
    # keyMap = "fr-mac";
  };

  services = {
    xserver = {
      # xkbVariant = "mac";
      enable = true;
      layout = "us";
      desktopManager.xfce.enable = true;
      displayManager.lightdm.enable = true;
    };

    dbus = {
      enable = true;
      packages = [ pkgs.dconf ];
    };

  };

  sound.enable = true;
  hardware.pulseaudio.enable = true;

  environment.systemPackages = with pkgs; [
    cachix
    chromium
    file
    git
    htop
    pavucontrol
    tmux
    tree
    vim 
  ];

  system.stateVersion = "22.05"; 

}

