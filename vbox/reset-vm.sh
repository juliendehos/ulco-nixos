#!/bin/sh

VM_NAME=nixos_${HOSTNAME}_${USER}

VBoxManage setproperty machinefolder /usr/local/VM/$USER
VBoxManage natnetwork add --netname NatNetwork --network "10.0.2.0/24"
# VBoxManage unregistervm --delete nixos
VBoxManage import /usr/local/VM/nixos.ova --vmname ${VM_NAME}
VBoxManage modifyvm ${VM_NAME} --nic1 natnetwork --nicpromisc1 allow-vms

# https://www.virtualbox.org/manual/ch08.html

