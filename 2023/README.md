

```nix
# /etc/nixos/configuration.nix

nix = {
  package = pkgs.nixFlakes;
  extraOptions = ''
    experimental-features = nix-command flakes
  '';
};
```

```sh
# sudo nixos-rebuild switch
```

```sh
$ nix run home-manager/release-23.05 -- init --switch
```

```nix
# ~/.config/home-manager/flake.nix

pkgs = import nixpkgs {
  inherit system;
  config.allowUnfree = true;
};
```

```nix
# ~/.config/home-manager/home.nix

  nixpkgs.overlays = [ 
    (import ./overlays/kickstart.nix)
    (import ./overlays/opencv.nix)
    (import ./overlays/vimPlugins.nix)
  ];
```

~/.config/home-manager/...

```sh
$ home-manager switch
```

vscode:

    - trusted workspace
    - switch terminal -> ctrl+;
    - updates


