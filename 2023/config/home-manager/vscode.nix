{ pkgs, ... }:

{

  programs.vscode = {
    enable = true;

    extensions = with pkgs.vscode-extensions; [

      bbenoist.nix
      ms-vscode.cpptools
      ms-python.python

      (pkgs.vscode-utils.buildVscodeMarketplaceExtension {
        mktplcRef = {
          name = "nix-env-selector";
          publisher = "arrterian";
          version = "1.0.9";
          sha256 = "sha256-TkxqWZ8X+PAonzeXQ+sI9WI+XlqUHll7YyM7N9uErk0=";
        };
      })

      (pkgs.vscode-utils.buildVscodeMarketplaceExtension {
        mktplcRef = {
          name = "cmake-tools";
          publisher = "ms-vscode";
          version = "1.16.1";
          sha256 = "sha256-JsEFZWtTvl0WZcmbasSc6+zImTCScgzIO3J5NkRdRf4=";
        };
      })

      (pkgs.vscode-utils.buildVscodeMarketplaceExtension {
        mktplcRef = {
          name = "cmake";
          publisher = "twxs";
          version = "0.0.17";
          sha256 = "sha256:11hzjd0gxkq37689rrr2aszxng5l9fwpgs9nnglq3zhfa1msyn08";
        };
      })

      (pkgs.vscode-utils.buildVscodeMarketplaceExtension {
        mktplcRef = {
          name = "vscode-ghc-simple";
          publisher = "dramforever";
          version = "0.2.3";
          sha256 = "sha256:1pd7p4xdvcgmp8m9aymw0ymja1qxvds7ikgm4jil7ffnzl17n6kp";
        };
      })

      (pkgs.vscode-utils.buildVscodeMarketplaceExtension {
        mktplcRef = {
          name = "language-haskell";
          publisher = "JustusAdam";
          version = "3.6.0";
          sha256 = "sha256-rZXRzPmu7IYmyRWANtpJp3wp0r/RwB7eGHEJa7hBvoQ=";
        };
      })

      # (pkgs.vscode-utils.buildVscodeMarketplaceExtension {
      #   mktplcRef = {
      #     name = "restructuredtext";
      #     publisher = "lextudio";
      #     version = "190.1.15";
      #     sha256 = "sha256-Gv5ciucAUT60P25ydkDODp48DW57NOCQOGfwtca99hE=";
      #   };
      # })

        (pkgs.vscode-utils.buildVscodeMarketplaceExtension {
          mktplcRef = {
            name = "todo-tree";
            publisher = "Gruntfuggly";
            version = "0.0.226";
            sha256 = "sha256-Fj9cw+VJ2jkTGUclB1TLvURhzQsaryFQs/+f2RZOLHs=";
          };
        })

    ];
  };
}

