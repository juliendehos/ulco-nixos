with import <nixpkgs> {};

stdenv.mkDerivation {
    name = "hellocpp";
    src = ./.;
    nativeBuildInputs = [ cmake ];
}
