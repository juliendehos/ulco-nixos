{ pkgs, ... }:

{

  programs.git = {
    enable = true;
    package = pkgs.gitAndTools.gitFull;
    userName = "TODO";
    userEmail = "TODO";

    ignores =  [
      ".*~"
      "*.swp"
      ".envrc"
      ".vscode"
      "build*"
      "dist-*"
    ];

    extraConfig = {
      pull.rebase = false;
      credential.helper = "cache --timeout=10800";
    };

  };

}

