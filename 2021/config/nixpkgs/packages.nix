{ pkgs, ... }:

  {

  home.packages = with pkgs; [

    kickstart

    (haskellPackages.ghcWithPackages (ps: with ps; [
      haskell-gi-base
      hspec
      gi-cairo
      gi-gdk
      gi-glib
      gi-gtk
      gi-cairo-render
      gi-cairo-connector
      QuickCheck
      random
      scotty
      split
      text
    ]))

    (python3.withPackages (ps: with ps; [
      pygments
      sphinx
      sphinx_rtd_theme
      (buildPythonPackage rec {
        pname = "rstcheck";
        version = "3.3.1";
        src = pkgs.fetchFromGitHub {
          owner = "myint";
          repo = "${pname}";
          rev = "v${version}";
          sha256 = "0wl5mlc7b8sifn5s3c5wv0ga1b99xf7ni6ig186dabpywhv48270";
        };
        doCheck = false;
        propagatedBuildInputs = [ docutils ];
      })
    ]))


    baobab

    cabal-install
    cmake
    ctags

    doxygen_gui

    evince

    gcc
    gdb
    gimp
    gnome3.eog
    gnumake
    gnuplot
    
    heroku
    hlint

    imagemagick

    killall
    kcachegrind

    libreoffice

    nodejs

    opencv_gtk

    pkg-config

    ranger

    sqlite
    sqlitebrowser

    tokei

    unzip

    valgrind
    vlc

    xorg.xkill

  ];

}

