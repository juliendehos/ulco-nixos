self: super: {

  opencv_gtk = super.opencv.override {
    enableGtk2 = true; 
    enableGStreamer = false;
    enableFfmpeg = true;
    enableTIFF = false;
    enableEXR = false;
    enableEigen = false;
    enableContrib = false;
  };

}

