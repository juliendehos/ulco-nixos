self: super: {

  kickstart = self.rustPlatform.buildRustPackage rec {
    pname = "kickstart";
    version = "v0.3.0";

    src = self.fetchFromGitHub {
      owner = "Keats";
      repo = pname;
      rev = version;
      sha256 = "sha256:1b2s4njx05qsgm366r01h032q8xfm7hyijlfam5dk0xvpzs0dpqy";
    };

    doCheck = false;
    cargoSha256 = "sha256:16vnba2g1kkzm0hnbqja5wv4j1bbq151mp39f7z5kvi14pjhjrfp";

  };

}

